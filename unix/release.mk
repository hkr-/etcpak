ARCH := $(shell uname -m)

ifeq ($(shell $(CC) -v 2>&1 | grep -c "clang"), 1)
  COMPILERNAME := clang
else
  COMPILERNAME := gcc
endif

CFLAGS := -O3 -fomit-frame-pointer
ifeq ($(COMPILERNAME),gcc)
  CFLAGS += -s
endif

DEFINES := -DNDEBUG

ifeq ($(ARCH),x86_64) 
  CFLAGS += -msse4.1
  ifeq ($(COMPILERNAME),clang)
    CFLAGS += -mavx2 -mbmi2 -mfma
  endif
endif

include build.mk
